// Import Application class that is the main part of our PIXI project
import { Application } from '@pixi/app'

// In order that PIXI could render things we need to register appropriate plugins
import { Renderer } from '@pixi/core' // Renderer is the class that is going to register plugins

import { BatchRenderer } from '@pixi/core' // BatchRenderer is the "plugin" for drawing sprites
Renderer.registerPlugin('batch', BatchRenderer)

// And just for convenience let's register Loader plugin in order to use it right from Application instance like app.loader.add(..) etc.
import { AppLoaderPlugin } from '@pixi/loaders'
Application.registerPlugin(AppLoaderPlugin)

// Sprite is our image on the stage
import { Sprite } from '@pixi/sprite'

// import "pixi-layers"; //or 
import * as PIXI from "pixi.js";
global.PIXI = PIXI;
require("pixi-layers")

// App with width and height of the page
const app = new Application({
	width: window.innerWidth,
	height: window.innerHeight
})
document.body.appendChild(app.view) // Create Canvas tag in the body
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);

//lives
var lives = 3, score = 0, messageLives, messageScore;
// Load the Assets
app.loader
	.add('bg',"./assets/background.jpg")
app.loader.load(() => {
	// background
	var texture = PIXI.Texture.from('bg');
	var background = Sprite.from(texture);
	app.stage.addChild(background);
	background.width = window.innerWidth;
	background.height = window.innerHeight;

	createbucket();
	showBall();

	//Display Text
	var style = new PIXI.TextStyle({
		fontFamily: "Roboto",
		fontSize: 40,
		fill: "white",
		stroke: '#000000',
		strokeThickness: 4,
	});
	
	messageLives = new PIXI.Text("Lives: 3", style);
	messageLives.anchor.set(1);
	messageLives.x = messageLives.width;
	messageLives.y = app.renderer.screen.height;
	messageScore = new PIXI.Text("Score: 0", style);
	messageScore.anchor.set(0.5, 1);
	messageScore.x = app.renderer.screen.width - messageLives.width + 60;
	messageScore.y = app.renderer.screen.height;
	app.stage.addChild(messageLives);
	app.stage.addChild(messageScore);
})
// create our little bucket friend..
var bucket = Sprite.from('./assets/bucket.png');
function createbucket(){
	// enable the bucket to be interactive... this will allow it to respond to mouse and touch events
	bucket.interactive = true;

	// this button mode will mean the hand cursor appears when you roll over the bucket with your mouse
	bucket.buttonMode = true;

	// center the bucket's anchor point
	bucket.anchor.set(0.5);

	// make it a bit bigger, so it's easier to grab
	bucket.scale.set(0.05);

	// setup events
	bucket
		// events for drag start
		.on('mousedown', onDragStart)
		.on('touchstart', onDragStart)
		// events for drag end
		.on('mouseup', onDragEnd)
		.on('mouseupoutside', onDragEnd)
		.on('touchend', onDragEnd)
		.on('touchendoutside', onDragEnd)
		// events for drag move
		.on('mousemove', onDragMove)
		.on('touchmove', onDragMove);

	// move the sprite to its designated position
	bucket.position.x = app.renderer.width/2;
	bucket.position.y = app.renderer.height - bucket.height;

	// add it to the stage
	app.stage.addChild(bucket);
}

requestAnimationFrame( animate );

function animate() {
	requestAnimationFrame(animate);
}

function onDragStart(event)
{
	// store a reference to the data
	// the reason for this is because of multitouch
	// we want to track the movement of this particular touch
	this.data = event.data;
	this.alpha = 0.5;
	this.dragging = true;
}

function onDragEnd(){
	this.alpha = 1;
	this.dragging = false;
	// set the interaction data to null
	this.data = null;
}

function onDragMove(){
	if (this.dragging){
		var newPosition = this.data.getLocalPosition(this.parent);
		this.position.x = newPosition.x;
	}
}

//beachballs
// create a new Sprite that uses the image name that we just generated as its source

// create a bounding box for the little beachballs
// const beachballBoundsPadding = 100;
const beachballBounds = new PIXI.Rectangle(0, 0, app.screen.width, app.screen.height);
var texture2 = new PIXI.Texture.from('./assets/beach-ball.png');
var beachball = Sprite.from(texture2);

function showBall(){
	// set the anchor point so the texture is centerd on the sprite
	beachball.anchor.set(0.5);

	// set a random scale for the beachball - no point them all being the same size!
	beachball.scale.set(0.05);

	// finally lets set the beachball to be at a random position..
	beachball.x = Math.random() * app.screen.width;
	beachball.y = 1;

	// create some extra properties that will control movement :
	// create a random direction in radians. This is a number between 0 and PI*2 which is the equivalent of 0 - 360 degrees
	beachball.direction = Math.PI * 2;

	// this number will be used to modify the direction of the beachball over time
	beachball.turningSpeed = Math.random() - 0.8;

	// create a random speed for the beachball between 0 - 2
	beachball.speed = Math.random() * 2;
	app.stage.addChild(beachball);
}

app.ticker.add(() => {
	beachball.direction += beachball.turningSpeed * 0.01;
	beachball.x += beachball.direction * beachball.speed;
	beachball.y += beachball.direction * beachball.speed;
	beachball.vx += 0.2;
	beachball.vy += 0.2;
	beachball.rotation = -beachball.direction - Math.PI / 2;

	// wrap the beachballs by testing their bounds...
	if (beachball.x < beachballBounds.x) {
		beachball.x -= beachballBounds.width;
	} else if (beachball.x > beachballBounds.x + beachballBounds.width) {
		beachball.x = beachballBounds.width;
	}

	if (beachball.y < beachballBounds.y) {
		beachball.y -= beachballBounds.height;
	} else if (beachball.y > beachballBounds.y + beachballBounds.height) {
		beachball.y = 0;
		lives -= 1;
		if(lives == 0){
			var validate = confirm("Game Over! Try Again?");
			if (validate == true) {
				lives = 3;
				score = 0;
				messageLives.text = 'Lives: ' + lives;
				messageScore.text = 'Score: ' + score;
			}
		} else {
			app.stage.removeChild(beachball)
			showBall()
			messageLives.text = 'Lives: ' + lives;
		}
	} else if (hitTestRectangle(beachball, bucket)) {
		app.stage.removeChild(beachball)
		showBall();
	} 
});

function hitTestRectangle(r1, r2) {
	//Define the variables we'll need to calculate
	let hit, boundsHitWidth, boundsHitHeight, vx, vy;
  
	//hit will determine whether there's a collision
	hit = false;
  
	//Find the center points of each sprite
	r1.centerX = r1.x + r1.width / 2;
	r1.centerY = r1.y + r1.height / 2;
	r2.centerX = r2.x + r2.width / 2;
	r2.centerY = r2.y + r2.height / 2;
  
	//Calculate the distance vector between the sprites
	vx = r1.centerX - r2.centerX;
	vy = r1.centerY - r2.centerY;
  
	//Figure out the combined half-widths and half-heights
	boundsHitWidth = 20;
	boundsHitHeight = 40;
  
	//Check for a collision on the x axis
	if (Math.abs(vx) < boundsHitWidth) {
	  //A collision might be occurring. Check for a collision on the y axis
	  if (Math.abs(vy) < boundsHitHeight) {
			score += 1;
			messageScore.text = 'Score: ' + score;
			hit = true;
	  } else {
			//There's no collision on the y axis
			hit = false;
	  }
	} else {
			//There's no collision on the x axis
			hit = false;
	}
  
	//`hit` will be either `true` or `false`
	return hit;
};